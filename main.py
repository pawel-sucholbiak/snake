import pygame

from ColorHelper import ColorHelper
from FoodContainer import FoodContainer
from Game import Game

pygame.init()
size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Python Snake")

game = Game()
game.game_size = size
game.window_size = (700, 500)

game.food_container = FoodContainer(food_count=10, food_size=10, max_game_point=size)

done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

move_counter = 0
move_every_n_ticks = 20

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        keys_pressed = pygame.key.get_pressed()
        game.steer_snake(keys_pressed)

    # --- Game logic should go here
    if not game.is_over:
        move_counter += 1
        if move_counter >= move_every_n_ticks:
            move_counter = 0
            game.tick()
            if game.should_speed_up():
                move_every_n_ticks -= 1
    # --- Screen-clearing code goes here

    screen.fill(ColorHelper.WHITE)

    # --- Drawing code should go here
    game.snake.draw_on(screen)
    game.food_container.draw_food_on(screen)
    game.draw_score(screen)

    if game.is_over:
        game.draw_game_over(screen)
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)  # Close the window and quit.
pygame.quit()
