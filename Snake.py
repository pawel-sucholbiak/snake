import pygame

from ColorHelper import ColorHelper


class Snake(object):
    def __init__(self):
        self.square_size = 10
        self.tail = [(5, 5), (5, 6), (5, 7), (5, 8), (5, 9)]
        self.default_length = len(self.tail)
        self.direction = (1, 0)
        self.ate_recently = False

    def draw_on(self, canvas):
        for x, y in self.tail:
            dimensions = [self.square_size * x, self.square_size * y, self.square_size, self.square_size]
            pygame.draw.rect(canvas, ColorHelper.LIGHT_BLUE, dimensions, )
            pygame.draw.rect(canvas, ColorHelper.BLACK, dimensions, 2)

    def move(self):
        pos_x, pos_y = self.tail[0]
        dir_x, dir_y = self.direction
        self.tail.insert(0, (pos_x + dir_x, pos_y + dir_y))
        del self.tail[-1]

    def is_over_the_edge(self, size):
        max_x, max_y = size
        pos_x, pos_y = self.tail[0]

        # check right border
        # check left border
        # check upper border
        # check bottom border
        if pos_x * self.square_size > max_x \
                or pos_x < 0 \
                or pos_y < 0 \
                or pos_y * self.square_size > max_y:
            return True
        return False

    def change_direction(self, direction):
        self.direction = direction

    def is_head_on_position(self, position):
        return self.tail[0] == position

    def add_tail_segment(self):
        self.tail.append(self.tail[-1])

    def is_biting_itself(self):
        for segment_pos in self.tail[1::]:
            if self.is_head_on_position(segment_pos):
                return True
        return False
