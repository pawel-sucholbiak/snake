import pygame

from ColorHelper import ColorHelper
from Direction import Direction
from FoodContainer import FoodContainer
from Snake import Snake


class Game(object):
    def __init__(self):
        self.game_size = (100, 100)
        self.food_container = FoodContainer(food_count=0)
        self.snake = Snake()
        self.square_size = 10
        self.score = 0
        self.is_over = False
        self.window_size = (0, 0)

    def draw_game_over(self, canvas):
        font = pygame.font.SysFont("monospace", 56)
        label = font.render("GAME OVER !!!", 1, ColorHelper.RED)
        x, y = self.window_size

        canvas.blit(label, (x / 6, y / 3))

    def draw_score(self, canvas):
        font = pygame.font.SysFont("monospace", 30)
        label = font.render('{:0>4}'.format(self.score), 1, ColorHelper.BLACK)
        x, y = self.window_size

        canvas.blit(label, (10, y - 40))

    def tick(self):
        self.snake.ate_recently = False
        self.snake.move()
        if self.snake.is_over_the_edge(self.game_size):
            self.is_over = True
        if self.snake.is_biting_itself():
            self.is_over = True
        # if snake.is_head_on_position(food.position):
        for food in [f for f in self.food_container.food_list if self.snake.is_head_on_position(f.position)]:
            self.snake.add_tail_segment()
            self.snake.ate_recently = True
            self.score += food.score_value
            food.set_random_position()

    def steer_snake(self, keys_pressed):
        direction = None

        if keys_pressed[pygame.K_LEFT]:
            direction = Direction.LEFT
        elif keys_pressed[pygame.K_RIGHT]:
            direction = Direction.RIGHT
        elif keys_pressed[pygame.K_UP]:
            direction = Direction.UP
        elif keys_pressed[pygame.K_DOWN]:
            direction = Direction.DOWN

        if direction:
            self.snake.change_direction(direction)

    def should_speed_up(self):
        snake_size = len(self.snake.tail)
        if snake_size % 5 == 0 and self.snake.ate_recently:
            return True
        return False
