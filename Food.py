import random

import pygame

from ColorHelper import ColorHelper


class Food(object):
    def __init__(self):
        self.square_size = 10
        self.position = (0, 0)
        self.absolute_max_position = (0, 0)
        self.score_value = 1

    def set_square_size(self, square_size):
        self.square_size = square_size
        return self

    def draw_on(self, canvas):
        x, y = self.position
        dimensions = [self.square_size * x, self.square_size * y, self.square_size, self.square_size]
        pygame.draw.rect(canvas, ColorHelper.GREEN, dimensions, )
        pygame.draw.rect(canvas, ColorHelper.BLACK, dimensions, 2)

    def set_position(self, position):
        self.position = position
        return self

    def set_absolute_max_position(self, absolute_max_position):
        self.absolute_max_position = absolute_max_position
        return self

    def set_random_position(self):
        max_x, max_y = self.absolute_max_position
        x = random.randint(0, int(max_x / self.square_size))
        y = random.randint(0, int(max_y / self.square_size))
        self.set_position((x, y))
        return self
